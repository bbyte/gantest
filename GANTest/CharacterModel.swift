//
//  CharacterModel.swift
//  GANTest
//
//  Created by Nikola Kotarov on 28.11.19.
//  Copyright © 2019 Exclus. All rights reserved.
//

import Foundation
import SwiftyJSON
import Realm
import RealmSwift

class CharacterModel: Object {
    
    @objc private dynamic var _charId: Int = 0
    @objc private dynamic var _created: Date = Date()
    @objc private dynamic var _name: String = ""
    @objc private dynamic var _birthday: Date = Date()
          private var _occupation = List<String>()
    @objc private dynamic var _imageURL: String = ""
    @objc private dynamic var _status: String = ""
    @objc private dynamic var _nickname: String = ""
          private var _appearance = List<Int>()
    @objc private dynamic var _portrayed: String = ""

    var created: Date { get { guard !self.isInvalidated else { return Date() }; return _created } set { guard !self.isInvalidated else { return }; _created = newValue } }
    var charId: Int { get { guard !self.isInvalidated else { return 0 }; return _charId } set { guard !self.isInvalidated else { return }; _charId = newValue } }
    var name: String { get { guard !self.isInvalidated else { return "" }; return _name } set { guard !self.isInvalidated else { return }; _name = newValue } }
    var birthday: Date { get { guard !self.isInvalidated else { return Date() }; return _birthday } set { guard !self.isInvalidated else { return }; _birthday = newValue } }
    var occupation: List<String>  { get { guard !self.isInvalidated else { return List<String>() }; return _occupation } set { guard !self.isInvalidated else { return }; _occupation = newValue } }
    var imageURL: String { get { guard !self.isInvalidated else { return "" }; return _imageURL } set { guard !self.isInvalidated else { return }; _imageURL = newValue } }
    var status: String { get { guard !self.isInvalidated else { return "" }; return _status } set { guard !self.isInvalidated else { return }; _status = newValue } }
    var nickname: String { get { guard !self.isInvalidated else { return "" }; return _nickname } set { guard !self.isInvalidated else { return }; _nickname = newValue } }
    var appearance: List<Int> { get { guard !self.isInvalidated else { return List<Int>() }; return _appearance } set { guard !self.isInvalidated else { return }; _appearance = newValue } }
    var portrayed: String { get { guard !self.isInvalidated else { return "" }; return _portrayed } set { guard !self.isInvalidated else { return }; _portrayed = newValue } }
    
    convenience init (_ data: JSON?) {
        self.init()
        
        self.charId = data?.dictionaryValue["char_id"]?.int ?? 0
        self.created = Date()
        self.name = data?.dictionaryValue["name"]?.string ?? ""
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-YYYY"
        self.birthday = formatter.date(from: (data?.dictionaryValue["birthday"]?.string) ?? "") ?? Date()
        _ = (data?.dictionaryValue["occupation"]?.array ?? [] ).map { self._occupation.append($0.string ?? "") }
        self.imageURL = data?.dictionaryValue["img"]?.string ?? ""
        self.status = data?.dictionaryValue["status"]?.string ?? ""
        self.nickname = data?.dictionaryValue["nickname"]?.string ?? ""
        _ = (data?.dictionaryValue["appearance"]?.array ?? []).map { self._appearance.append($0.int ?? 0) }
        self.portrayed = data?.dictionaryValue["portrayed"]?.stringValue ?? ""
    }
    
    override static func primaryKey() -> String? {
        return "_charId"
    }
}
