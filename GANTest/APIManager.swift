//
//  APIManager.swift
//  GANTest
//
//  Created by Nikola Kotarov on 28.11.19.
//  Copyright © 2019 Exclus. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias GeneralCompletionHandler = (JSON?, _ error: Error?) -> Void

class APIManager {
    
    static let sharedInstance = APIManager()
    
    // List of all endpoinds. Currently there is only one
    enum endPointCall: CaseIterable {
        case Characters
    }
    
    // environment endpoint types we have
    public enum endPointType: String, CaseIterable {
        case Production = "Production", Development = "Development"
    }
    
    let endPointBaseURL: [endPointType: String] = [
        .Production: "https://breakingbadapi.com/api/characters"
    ]
    
    var currentEndpointType: endPointType {
        get {
            return APIManager.endPointType(rawValue: UserDefaults.standard.string(forKey: "currentEndPoint") ?? "Production") ?? .Production
        }
        
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: "currentEndPoint")
            UserDefaults.standard.synchronize()
        }
    }
    
    struct endPoint {
        var call: endPointCall = .Characters
        var path: String = ""
        var secure: Bool = false                   // everything is public
        var method: HTTPMethod = .get              // default behavior is get
        var multipart: Bool = false                // default is not multipart
        var url: String { return APIManager.sharedInstance.endPointBaseURL[APIManager.sharedInstance.currentEndpointType]! + "/" + path }
        
        init () {}
        
        init (call: endPointCall, path: String) {
            self.call = call
            self.path = path
        }
    }
    
    // actual list of endpoints
    private let endPoints: [endPoint] = [
        endPoint(call: .Characters, path: "")
    ]
    
    private func get(_ call: endPointCall) -> endPoint {
        let returnedEndPoints = self.endPoints.filter { endPoint in endPoint.call == call }
        return returnedEndPoints.count > 0 ? returnedEndPoints[0] : endPoint()
    }
        
    func getCharacters(_ completionHandler: @escaping GeneralCompletionHandler) {
        let endPoint = get(.Characters)
        
        request(endPoint.url, method: endPoint.method).validate().responseJSON { (response) in
            
            if let error = response.error {
                
                print(error.localizedDescription)
                completionHandler(nil, error)
            } else {
                
                let jsonData = JSON(response.data as Any)
                completionHandler(jsonData, nil)
            }
        }
    }
}
