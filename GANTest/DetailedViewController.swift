//
//  DetailedViewController.swift
//  GANTest
//
//  Created by Nikola Kotarov on 28.11.19.
//  Copyright © 2019 Exclus. All rights reserved.
//

import UIKit
import Kingfisher

class DetailedViewController: UIViewController {
    
    var character: CharacterModel = CharacterModel()
    
    @IBOutlet weak var seasonsLabel: UILabel!
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var occupationLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var pictureImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        
        pictureImageView.kf.setImage(with: URL(string: character.imageURL))
        nameLabel.text = character.name
        occupationLabel.text = String(character.occupation.reduce ("", {$0 + $1 + " ,"}).dropLast())
        statusLabel.text = character.status
        nicknameLabel.text = character.nickname
        seasonsLabel.text = String(character.appearance.reduce ("", {String("\($0)\($1) ,")}).dropLast())
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
