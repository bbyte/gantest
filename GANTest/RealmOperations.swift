//
//  RealmOperations.swift
//  GANTest
//
//  Created by Nikola Kotarov on 28.11.19.
//  Copyright © 2019 Exclus. All rights reserved.
//

import Foundation

import Realm
import RealmSwift

class RealmOperations {

    static let sharedInstance = RealmOperations()

    var realm: Realm? {
        do {
            return try Realm()
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func saveCharacters(_ characters: [CharacterModel]?) {
        if(characters != nil) {
            try! CharacterModel.deleteAll(in: try! Realm())
            for character in characters! {
                saveCharacter(character)
            }
        }
    }

    func saveCharacter(_ character: CharacterModel?) {
        if(character != nil) {
            let realm = try! Realm()
            try! realm.write {
                realm.create(CharacterModel.self, value: character!, update: .all)
            }
        }
    }

    func getCharacters() -> [CharacterModel]? {
        let realm = try! Realm()

        let characters = realm.objects(CharacterModel.self) // get all characters
        if(characters.count > 0) {
            return Array(characters)
        }
        return [CharacterModel]()
    }    
}
