//
//  MainViewController.swift
//  GANTest
//
//  Created by Nikola Kotarov on 28.11.19.
//  Copyright © 2019 Exclus. All rights reserved.
//

import UIKit
import Kingfisher

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var characters: [CharacterModel] = []
    let grayView = UIView()
    let activityIndicatorView = UIActivityIndicatorView(style: .large)
    
    @IBOutlet var seasonButtons: [UIButton]!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    var currentSeason: Int = 0 {
        didSet {
            searchCharacters()
        }
    }
    var searchText: String = "" {
        didSet {
            searchCharacters()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
                            
        hideKeyboardWhenTappedAround(assignedView: view)
        
        // default is all seasons
        seasonButtons.filter { $0.tag == 0 }.last!.setTitleColor(.red, for: .normal)
                
        grayView.backgroundColor = .black
        grayView.alpha = 0.6
        grayView.frame = view.bounds
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.center = grayView.center
        grayView.addSubview(activityIndicatorView)

        characters = RealmOperations.sharedInstance.getCharacters() ?? [CharacterModel]()

        // show only if there are no characters in the cache
        if characters.count <= 0 {
            view.addSubview(grayView)
            activityIndicatorView.startAnimating()
        }

        APIManager.sharedInstance.getCharacters { [weak self] (data, error) in
            self?.grayView.removeFromSuperview()
            guard let self = self else { return }
            if let error = error {
                self.showAlert(error.localizedDescription, title: "Error!")
            } else {
                let newCharacters = data?.arrayValue.map { return CharacterModel($0) }
                RealmOperations.sharedInstance.saveCharacters(newCharacters)
                self.characters = newCharacters ?? []
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }

    
    // MARK: - UI action functions
    @IBAction func seasonButtonToched(_ sender: Any) {
        
        _ = seasonButtons.map { $0.setTitleColor(.black, for: .normal) }
        seasonButtons.filter { $0.tag == (sender as AnyObject).tag }.last!.setTitleColor(.red, for: .normal)
        
        currentSeason = (sender as AnyObject).tag
    }
        
    @IBAction func searchTextFieldChanged(_ sender: Any) {
        searchText = searchTextField.text!
    }
        
    // MARK: - UITableView Delegate & Data source functions
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        characters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "characterTableViewCell", for: indexPath) as! CharacterTableViewCell
                
        cell.pictureImageView.kf.setImage(with: URL(string: characters[indexPath.row].imageURL))
        cell.nameLabel.text = characters[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "detailedViewController") as! DetailedViewController
        
        viewController.character = characters[indexPath.row]
                
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    // MARK: - Private functions
    private func searchCharacters() {
        var newCharacters = RealmOperations.sharedInstance.getCharacters()!
        if !searchText.isEmpty {
            newCharacters = newCharacters.filter { $0.name.localizedCaseInsensitiveContains(searchTextField!.text!) }
        }
        
        if currentSeason != 0 {
            newCharacters = newCharacters.filter { $0.appearance.contains(currentSeason) }
        }
        
        characters = newCharacters
        tableView.reloadData()
    }
        
    private func showAlert(_ message: String, title: String = "Alert") {
        if(UIApplication.shared.applicationState != .background) {
            let newMessage = message
            let alert = UIAlertController(title: title, message: newMessage, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
